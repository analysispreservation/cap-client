FROM python:3.8-slim-buster

WORKDIR /code

# Update aptitude with new repo
RUN apt-get update

# Install software 
RUN apt-get install -y git curl


RUN git clone https://github.com/cernanalysispreservation/cap-client.git .

RUN pip install .[all]
